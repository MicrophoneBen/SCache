package org.scache;

/**
 * 分布式锁
 *
 * @author junfeng
 * @since 15/7/9 13:29
 */
public interface ILock {
    /**
     * 尝试获取锁
     *
     * @return
     */
    boolean tryLock();

    /**
     * 释放锁
     */
    void unlock();
}
