
package org.scache;

/**
 * 有生命周期的Service,可以有自己的初始化(init)和资源回收(destory)方法
 *
 * @author 君枫
 * @version 1.0
 * @since 2014-8-22 下午3:30:01
 */
public interface Initializable {

    /**
     * 初始化方法
     */
    void init();

    /**
     * 回收资源。
     */
    void destroy();
}