package org.scache.util;

/**
 * 类型工具类
 *
 * @author junfeng
 * @since 15/7/6 16:48
 */
public abstract class ClassUtil {
    public static boolean isPrimitiveOrWrapper(Class<?> clazz) {
        return clazz.isPrimitive()
                || clazz.equals(Boolean.class)
                || clazz.equals(Byte.class)
                || clazz.equals(Character.class)
                || clazz.equals(Double.class)
                || clazz.equals(Float.class)
                || clazz.equals(Integer.class)
                || clazz.equals(Long.class)
                || clazz.equals(Short.class);
    }
}
