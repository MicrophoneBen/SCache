package org.scache.util;

import java.util.Collection;
import java.util.Map;

/**
 * 集合工具类
 * 
 * @author wangyajun
 * @since 2014-12-18 上午9:43:50
 */
public final class CollectionUtil {

    private CollectionUtil() {
    }

    public static boolean isNull(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotNull(Collection<?> collection) {
        return collection != null && !collection.isEmpty();
    }

    public static boolean isNull(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isNotNull(Map<?, ?> map) {
        return map != null && !map.isEmpty();
    }
}
