/*
 * Copyright 2015 Focus Technology, Co., Ltd. All rights reserved.
 */
package org.scache.util;

/**
 * String工具类
 * 
 * @author gulingfeng
 */
public final class StringUtil {
    private StringUtil() {
    }

    public static boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }

    public static boolean isNotBlank(String str) {
        return str != null && !str.trim().isEmpty();
    }

    /**
     * 根据分隔符拼接数组中数据
     * 
     * @return
     */
    public static String join(Object[] array, String separator) {
        if (array == null) {
            return null;
        }
        if (separator == null) {
            separator = "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            sb.append(array[i]);
            if (i < array.length - 1) {
                sb.append(separator);
            }
        }
        return sb.toString();
    }

}
