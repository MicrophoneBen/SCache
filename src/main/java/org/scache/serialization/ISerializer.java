
package org.scache.serialization;

import org.scache.serialization.hessian.Hessian2Serializer;
import org.scache.serialization.hessian.HessianSerializer;
import org.scache.serialization.json.FastJSONSerializer;

import java.io.Serializable;


/**
 * 序列化接口，实现要求<b>线程安全</b>
 * <ul>
 * <li>序列化实现有差异，建议序列化对象符合JavaBean规范（pojo）</li>
 * </ul>
 *
 * @author 君枫
 * @since 2014年3月10日 下午3:49:30
 */
public interface ISerializer {

    ISerializer JSONSerializer = new FastJSONSerializer();
    ISerializer HessianSerializer = new HessianSerializer();
    ISerializer Hessian2Serializer = new Hessian2Serializer();


    /**
     * 对象序列化
     *
     * @return 序列化后字节数组
     */
    byte[] serialize(Serializable s);

    /**
     * 对象反序列化
     *
     * @param bytes
     * @return 反序列化后的对象
     */
    <T> T deserialize(byte[] bytes);
}
