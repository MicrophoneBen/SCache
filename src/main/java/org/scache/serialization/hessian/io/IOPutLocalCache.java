package org.scache.serialization.hessian.io;

import com.caucho.hessian.io.*;

/**
 * io相关put {@link ThreadLocal}缓存
 *
 * @author wangyajun
 * @since 2015-1-6 上午11:38:53
 */
public interface IOPutLocalCache {
    SerializerFactory serializerFactory =new SerializerFactory();

    ThreadLocal<ReusableByteArrayInputStream> LOCAL_INPUT_STREAM = new ThreadLocal<ReusableByteArrayInputStream>() {

        @Override
        protected ReusableByteArrayInputStream initialValue() {
            return new ReusableByteArrayInputStream();
        }

    };

    ThreadLocal<ReusableByteArrayOutputStream> LOCAL_OUTPUT_STREAM = new ThreadLocal<ReusableByteArrayOutputStream>() {

        @Override
        protected ReusableByteArrayOutputStream initialValue() {
            return new ReusableByteArrayOutputStream();
        }

    };

    ThreadLocal<Hessian2Output> LOCAL_HESSIAN2_OUTPUT = new ThreadLocal<Hessian2Output>() {

        @Override
        protected Hessian2Output initialValue() {
            Hessian2Output h2o=new Hessian2Output();
            h2o.setSerializerFactory(serializerFactory);
            return h2o;
        }

    };

    ThreadLocal<Hessian2Input> LOCAL_HESSIAN2_INPUT = new ThreadLocal<Hessian2Input>() {

        @Override
        protected Hessian2Input initialValue() {
            Hessian2Input h2i=new Hessian2Input();
            h2i.setSerializerFactory(serializerFactory);
            return h2i;
        }

    };

    ThreadLocal<HessianOutput> LOCAL_HESSIAN_OUTPUT = new ThreadLocal<HessianOutput>() {

        @Override
        protected HessianOutput initialValue() {
            HessianOutput ho=new HessianOutput();
            ho.setSerializerFactory(serializerFactory);
            return ho;
        }

    };

    ThreadLocal<HessianInput> LOCAL_HESSIAN_INPUT = new ThreadLocal<HessianInput>() {

        @Override
        protected HessianInput initialValue() {
            HessianInput hi=new HessianInput();
            hi.setSerializerFactory(serializerFactory);
            return hi;
        }

    };
}
