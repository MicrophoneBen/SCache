package org.scache.serialization.hessian.io;

import java.io.ByteArrayOutputStream;

/**
 * 可复用 的{@link ByteArrayOutputStream}，利用 {@link ThreadLocal}达到复用效果
 *
 * @author wangyajun
 * @since 2015-1-6 上午11:26:37
 */
public class ReusableByteArrayOutputStream extends ByteArrayOutputStream {

    public void init() {
        reset();
    }
}
