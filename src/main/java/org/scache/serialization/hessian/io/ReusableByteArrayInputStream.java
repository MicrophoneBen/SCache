package org.scache.serialization.hessian.io;

import java.io.ByteArrayInputStream;

/**
 * 可复用 的{@link ByteArrayInputStream}，利用 {@link ThreadLocal}达到复用效果
 *
 * @author wangyajun
 * @since 2015-1-6 上午11:19:05
 */
public class ReusableByteArrayInputStream extends ByteArrayInputStream {

    ReusableByteArrayInputStream() {
        super(new byte[0]);
    }

    public void init(byte[] buf) {
        this.buf = buf;
        this.pos = 0;
        this.count = buf.length;
    }
}
