
package org.scache.serialization.hessian;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;
import org.scache.log.InternalLogger;
import org.scache.serialization.ISerializer;
import org.scache.serialization.hessian.io.IOPutLocalCache;
import org.scache.serialization.hessian.io.ReusableByteArrayInputStream;
import org.scache.serialization.hessian.io.ReusableByteArrayOutputStream;

import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

/**
 * Hessian序列化工具
 *
 * @author 君枫
 * @since 2014年3月12日 上午11:02:31
 */
public class HessianSerializer implements ISerializer {


    @Override
    public byte[] serialize(Serializable s) {
        if (s != null) {
            ReusableByteArrayOutputStream bos = IOPutLocalCache.LOCAL_OUTPUT_STREAM.get();
            bos.init();
            HessianOutput ho = IOPutLocalCache.LOCAL_HESSIAN_OUTPUT.get();
            ho.init(bos);
            try {
                ho.writeObject(s);
                return bos.toByteArray();
            } catch (IOException e) {
                InternalLogger.error("序列化对象" + s + "失败:" + e.getMessage());
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T deserialize(byte[] bytes) {
        if (bytes != null) {
            ReusableByteArrayInputStream bis = IOPutLocalCache.LOCAL_INPUT_STREAM.get();
            bis.init(bytes);
            HessianInput hi = IOPutLocalCache.LOCAL_HESSIAN_INPUT.get();
            hi.init(bis);
            try {
                return (T) hi.readObject();
            } catch (IOException e) {
                InternalLogger.error("反序列化对象失败:" + e.getMessage());
            }
        }
        return null;
    }

    public static class P implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -1134721494136082522L;
        static Random r = new Random();
        String x1 = "1" + r.nextDouble();
        String x2 = "2" + r.nextDouble();
        String x3 = "3" + r.nextDouble();
        String x12 = "1" + r.nextDouble();
        String x23 = "2" + r.nextDouble();
        String x34 = "3" + r.nextDouble();
        String x51 = "1" + r.nextDouble();
        String x26 = "2" + r.nextDouble();
        String xfddas3 = "3" + r.nextDouble();
        String xfdas1 = "1" + r.nextDouble();
        String xfddfassa2 = "2" + r.nextDouble();
        String xfdsdasfas3 = "3" + r.nextDouble();
        String x1fdagfs = "1" + r.nextDouble();
        String x2fdfas = "2" + r.nextDouble();
        String x3ffdas = "3" + r.nextDouble();
        String x1ffdfdas = "1" + r.nextDouble();
        String xfdagfsff2 = "2" + r.nextDouble();
        String x3fgfas = "3" + r.nextDouble();
        String x1dfas = "1" + r.nextDouble();
        String x2fdgfas = "2" + r.nextDouble();
        String xfdas3 = "3" + r.nextDouble();
        String x1fas = "1" + r.nextDouble();
        String xfdagfs2 = "2" + r.nextDouble();
        String x3ffgdas = "3" + r.nextDouble();
        String x1fdas = "1" + r.nextDouble();
        String xfdsa2 = "2" + r.nextDouble();
        String xfads3 = "3" + r.nextDouble();

        @Override
        public String toString() {
            return "P [x1=" + x1 + ", x2=" + x2 + ", x3=" + x3 + ", x12=" + x12 + ", x23=" + x23 + ", x34=" + x34 + ", x51="
                    + x51 + ", x26=" + x26 + ", xfddas3=" + xfddas3 + ", xfdas1=" + xfdas1 + ", xfddfassa2=" + xfddfassa2
                    + ", xfdsdasfas3=" + xfdsdasfas3 + ", x1fdagfs=" + x1fdagfs + ", x2fdfas=" + x2fdfas + ", x3ffdas=" + x3ffdas
                    + ", x1ffdfdas=" + x1ffdfdas + ", xfdagfsff2=" + xfdagfsff2 + ", x3fgfas=" + x3fgfas + ", x1dfas=" + x1dfas
                    + ", x2fdgfas=" + x2fdgfas + ", xfdas3=" + xfdas3 + ", x1fas=" + x1fas + ", xfdagfs2=" + xfdagfs2
                    + ", x3ffgdas=" + x3ffgdas + ", x1fdas=" + x1fdas + ", xfdsa2=" + xfdsa2 + ", xfads3=" + xfads3 + "]";
        }
    }

    public static void main(String[] args) {
        ISerializer serializer = ISerializer.Hessian2Serializer;
        System.out.println(serializer.getClass().getName());
        P p;
        byte[] bs;
        for (int i = 1; i < 2; i++) {
            p = new P();
            System.out.println("test " + i + "---------------------");
            long sTime = System.currentTimeMillis();
            bs = serializer.serialize(p);
            long eTime = System.currentTimeMillis();
            System.out.println("序列化耗时" + (eTime - sTime) + "ms，大小："+bs.length);
            sTime = System.currentTimeMillis();
            System.out.println(serializer.deserialize(bs));
            eTime = System.currentTimeMillis();
            System.out.println("反序列化耗时" + (eTime - sTime) + "ms");
        }
    }
}
