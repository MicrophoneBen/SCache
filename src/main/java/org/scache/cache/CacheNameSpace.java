
package org.scache.cache;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * 模块缓存命名空间，根据自身产品定义模块名称（public static final String）
 *
 * @author 君枫
 * @since 2014年3月12日 上午9:42:11
 */
public abstract class CacheNameSpace {

    public static final String CM = "cm";
    public static final String PM = "pm";
    public static final String FM = "am";
    public static final String CRON = "cron";
    public static final String REPORT = "report";
    /**
     * 模块命名空间集合
     */
    private static String[] nameSpaceValues;

    static {
        // namespace都是public修饰
        Field[] nameSpaceFields = CacheNameSpace.class.getFields();
        nameSpaceValues = new String[nameSpaceFields.length];
        for (int i = 0; i < nameSpaceFields.length; i++) {
            try {
                nameSpaceValues[i] = String.valueOf(nameSpaceFields[i].get(null));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 是否为预定义NameSpace
     *
     * @param nameSpace 待判断的namespace
     * @return
     */
    public static boolean isPredefineNameSpace(String nameSpace) {
        for (String predefineNameSpace : nameSpaceValues) {
            if (predefineNameSpace.equals(nameSpace))
                return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.asList(CacheNameSpace.nameSpaceValues));
        System.out.println(isPredefineNameSpace("AlarmManager"));
        System.out.println(isPredefineNameSpace("ss"));
    }
}
