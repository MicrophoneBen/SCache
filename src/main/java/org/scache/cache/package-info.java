/**
 * 缓存模块，包括本地（JVM进程内）和远程（Redis等缓存库），程序片段内的缓存推荐使用
 * {@link java.util.concurrent.ConcurrentHashMap}
 * 
 * @author 君枫
 * @since 2014年3月10日 下午3:48:06
 */

package org.scache.cache;