
package org.scache.cache;

import org.scache.ILock;
import org.scache.IdGenerator;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;


/**
 * 可序列化缓存管理接口,需要保证<b>线程安全</b>
 * <p/>
 * <pre>
 *  1 提供分布式标准的java数据结构
 *  2 提供分布式计数器
 *  3 提供分布式Lock实现
 *  4 提供 removeComponent、expireComponent等管理方法
 * </pre>
 *
 * @author 君枫
 * @since 2014年3月10日 下午4:02:50
 * <br>
 */
public interface ISerializableCache {
    char SEP = ':';

    /**
     * 模块类型
     */
    enum ModuleType {
        IDGENERATOR("idGenerator"),
        LIST("list"),
        CONCURRENT_SET("concurrentSet"),
        BLOCKING_QUEUE("blockingQueue"),
        CONCURRENT_MAP("concurrentMap"),
        LOCK("lock");

        final String moduleName;

        private ModuleType(String moduleName) {
            this.moduleName = moduleName;
        }

        public String getModuleName() {
            return moduleName;
        }
    }


    /**
     * 获取 ConcurrentMap
     *
     * @param mapName
     * @return
     */
    <K extends Serializable, V extends Serializable> ConcurrentMap<K, V> getConcurrentMap(String mapName);


    /**
     * 构建ID生成器
     *
     * @param idKey
     * @return
     */
    IdGenerator getIdGenerator(String idKey);


    /**
     * 获取listName对应的 {@link List}
     *
     * @return 如果不存在映射，返回空列表（非null）
     */
    <T extends Serializable> List<T> getList(String listName);


    /**
     * 获取  {@link BlockingQueue}
     *
     * @param queueName 队列名称
     * @return
     */
    <T extends Serializable> BlockingQueue<T> getBlockingQueue(String queueName);


    /**
     * 获取 {@link Set}
     *
     * @param setName
     * @return
     */
    <T extends Serializable> Set<T> getConcurrentSet(String setName);


    /**
     * 获取锁，分布式场景下，锁默认有默认超时时间1小时,如需调整锁有效时间调用 expireComponent
     *
     * @param lockName
     * @return
     */
    ILock getLock(String lockName);

    /**
     * 移除组件
     *
     * @param type
     * @param componentName list、Map、Queue等组件名称
     * @return
     */
    boolean removeComponent(ModuleType type, String componentName);

    /**
     * 设置组件的超时时间，超时后自动移除
     *
     * @param type
     * @param componentName list、Map、Queue等组件名称
     * @param unit
     * @param time
     * @return
     */
    boolean expireComponent(ModuleType type, String componentName, TimeUnit unit, long time);
}
