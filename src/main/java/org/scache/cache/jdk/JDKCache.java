
package org.scache.cache.jdk;

import org.scache.ILock;
import org.scache.IdGenerator;
import org.scache.cache.ISerializableCache;
import org.scache.util.ConcurrentHashSet;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * JDK缓存，对 {@link ConcurrentMap}简单包装
 *
 * @author 君枫
 * @since 2014-4-18 上午10:35:05
 */
@SuppressWarnings("unchecked")
class JDKCache implements ISerializableCache {

    private static Map<String, Object> componentMap = new ConcurrentHashMap<String, Object>();
    final String nameSpace;

    /**
     *
     */
    public JDKCache(String nameSpace) {
        this.nameSpace = nameSpace + SEP;
    }


    @Override
    public <K extends Serializable, V extends Serializable> ConcurrentMap<K, V> getConcurrentMap(String mapName) {
        String name = getComponentFullName(ModuleType.CONCURRENT_MAP, mapName);
        ConcurrentMap<K, V> map = (ConcurrentMap<K, V>) componentMap.get(name);
        if (map == null) {
            synchronized (componentMap) {
                map = (ConcurrentMap<K, V>) componentMap.get(name);
                if (map == null) {
                    map = new ConcurrentHashMap<K, V>();
                    componentMap.put(name, map);
                }
            }
        }
        return map;
    }


    @Override
    public IdGenerator getIdGenerator(String idKey) {
        return JDKIdGenerator.get(nameSpace, idKey);
    }

    @Override
    public <T extends Serializable> List<T> getList(String idKey) {
        String name = getComponentFullName(ModuleType.LIST, idKey);
        List<T> list = (List<T>) componentMap.get(name);
        if (list == null) {
            synchronized (componentMap) {
                list = (List<T>) componentMap.get(name);
                if (list == null) {
                    list = new ArrayList<T>();
                    list = Collections.synchronizedList(list);
                    componentMap.put(name, list);
                }
            }
        }
        return list;
    }


    @Override
    public <T extends Serializable> BlockingQueue<T> getBlockingQueue(String queueName) {
        String name = getComponentFullName(ModuleType.BLOCKING_QUEUE, queueName);
        BlockingQueue<T> queue = (BlockingQueue<T>) componentMap.get(name);
        if (queue == null) {
            synchronized (componentMap) {
                queue = (BlockingQueue<T>) componentMap.get(name);
                if (queue == null) {
                    queue = new LinkedBlockingQueue<T>();
                    componentMap.put(name, queue);
                }
            }
        }
        return queue;
    }


    @Override
    public <T extends Serializable> Set<T> getConcurrentSet(String setName) {
        String name = getComponentFullName(ModuleType.LIST, setName);
        Set<T> set = (Set<T>) componentMap.get(name);
        if (set == null) {
            synchronized (componentMap) {
                set = (Set<T>) componentMap.get(name);
                if (set == null) {
                    set = new ConcurrentHashSet<T>();
                    componentMap.put(name, set);
                }
            }
        }
        return set;
    }

    @Override
    public ILock getLock(String lockName) {
        String name = getComponentFullName(ModuleType.LOCK, lockName);
        ILock lock = (ILock) componentMap.get(name);
        if (lock == null) {
            synchronized (componentMap) {
                lock = (ILock) componentMap.get(name);
                if (lock == null) {
                    lock = new ILock() {
                        Lock lock = new ReentrantLock();

                        @Override
                        public boolean tryLock() {
                            return lock.tryLock();
                        }

                        @Override
                        public void unlock() {
                            lock.unlock();
                        }
                    };
                    componentMap.put(name, lock);
                }
            }
        }
        return lock;
    }

    String getComponentFullName(ModuleType type, String componentName) {
        return nameSpace + type.getModuleName() + SEP + componentName;
    }

    @Override
    public boolean removeComponent(ModuleType type, String componentName) {
        return componentMap.remove(getComponentFullName(type, componentName)) != null;
    }

    @Override
    public boolean expireComponent(ModuleType type, String componentName, TimeUnit unit, long time) {
        throw new UnsupportedOperationException();
    }


    /**
     * JDK缓存实现的id生成器
     *
     * @author 君枫
     * @version 1.0
     * @category com.linkage.toptea.util.cache.jdk<br>
     * @since 2014-4-27 下午4:33:44
     */
    static class JDKIdGenerator implements IdGenerator {

        static Map<String, JDKIdGenerator> idKey2IdGeneratorMap = new ConcurrentHashMap<String, JDKIdGenerator>();
        private final AtomicLong id = new AtomicLong();

        /**
         * 防止外部直接构造
         */
        private JDKIdGenerator() {
        }

        /**
         * 获取 {@link IdGenerator}
         *
         * @param nameSpace 模块命名空间
         * @param idKey
         * @return
         */
        static IdGenerator get(String nameSpace, String idKey) {
            String nameSpaceAndIdKey = nameSpace + idKey;
            JDKIdGenerator idGenerator = idKey2IdGeneratorMap.get(nameSpaceAndIdKey);
            if (idGenerator == null) {
                idGenerator = new JDKIdGenerator();
                idKey2IdGeneratorMap.put(nameSpaceAndIdKey, idGenerator);
            }
            return idGenerator;
        }

        @Override
        public String getCurrentId() throws Exception {
            return String.valueOf(id.get());
        }

        @Override
        public String generateId(boolean updateCurrentId) throws Exception {
            if (updateCurrentId) {
                return String.valueOf(id.incrementAndGet());
            }
            return String.valueOf((id.get() + 1));
        }

        @Override
        public boolean updateCurrentId(String targetId) throws Exception {
            id.set(Long.valueOf(targetId));
            return true;
        }

        @Override
        public boolean available() {
            return id != null;
        }
    }


    public static void main(String[] args) {
    }

    static void getGeneric() {
        JDKCache cache = new JDKCache("fm");
        Class<?> clazz = cache.getClass();
        Type[] types = clazz.getGenericInterfaces();
        for (Type type : types) {
            if (type instanceof ParameterizedType) {
                Type[] argumentTypes = ((ParameterizedType) type).getActualTypeArguments();
                for (Type argumentType : argumentTypes) {
                    System.out.println(argumentType);
                }
            }
        }
    }
}
