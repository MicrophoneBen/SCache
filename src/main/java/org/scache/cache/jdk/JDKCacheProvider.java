
package org.scache.cache.jdk;

import org.scache.cache.AbstractSerializableCacheProvider;
import org.scache.cache.ISerializableCache;
import org.scache.serialization.ISerializer;


/**
 * JDK缓存提供者，忽略序列化，泛指并发集合
 *
 * @author 君枫
 * @since 2014-4-18 上午10:28:08
 */
public class JDKCacheProvider extends AbstractSerializableCacheProvider {

    private final long id = System.currentTimeMillis();


    @Override
    public String getName() {
        return "JDKCacheProvider[In JVM Process][" + id + "]";
    }


    @Override
    public void init() {
    }


    @Override
    protected ISerializableCache buildCache(String namespace, ISerializer serializer) {
        return new JDKCache(namespace);
    }
}
