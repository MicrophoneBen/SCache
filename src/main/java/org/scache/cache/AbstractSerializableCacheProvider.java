
package org.scache.cache;

import org.scache.serialization.ISerializer;

import java.util.concurrent.ConcurrentHashMap;


/**
 * 抽象缓存提供者
 *
 * @author 君枫
 * @since 2014-4-18 上午10:07:08
 */
@SuppressWarnings("rawtypes")
public abstract class AbstractSerializableCacheProvider implements ISerializableCacheProvider {

    protected ISerializer serializer;
    protected ConcurrentHashMap<String, ISerializableCache> nameSpaceCacheMap = new ConcurrentHashMap<String, ISerializableCache>();

    /*
     * (non-Javadoc)
     *
     * @see com.linkage.toptea.util.cache.ICacheProvider#getCache(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public ISerializableCache getCache(String namespace) {
        ISerializableCache cache = null;
        if (CacheNameSpace.isPredefineNameSpace(namespace)) {
            synchronized (namespace.intern()) {
                cache = nameSpaceCacheMap.get(namespace);
                if (cache == null) {
                    cache = buildCache(namespace, serializer);
                    nameSpaceCacheMap.put(namespace, cache);
                }
            }
        }
        return cache;
    }

    /**
     * 构建缓存
     *
     * @param namespace  缓存命名空间
     * @param serializer
     * @return
     */
    protected abstract ISerializableCache buildCache(String namespace, ISerializer serializer);

    @Override
    public void destroy() {
        nameSpaceCacheMap.clear();
    }

    /**
     * @param serializer the serializer to set
     */
    public void setSerializer(ISerializer serializer) {
        this.serializer = serializer;
    }
}
