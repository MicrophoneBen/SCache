
package org.scache.cache.redis;

import org.scache.serialization.ISerializer;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Protocol;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.scache.log.InternalLogger.error;
import static org.scache.log.InternalLogger.isErrorenabled;


/**
 * Redis的 {@link BlockingQueue}实现
 *
 * @author 君枫
 * @since 2014-8-18 下午3:00:45
 */
class RedisBlockingQueue<T extends Serializable> extends RedisList<T> implements BlockingQueue<T> {

    RedisBlockingQueue(String queueName, JedisPool jedisPool, ISerializer serializer) {
        super(queueName, jedisPool, serializer);
    }

    @Override
    public T remove() {
        return remove(0);
    }

    @Override
    public T poll() {
        try {
            return remove();
        } catch (Exception e) {

            return null;
        }
    }

    @Override
    public T element() {
        return get(0);
    }

    @Override
    public T peek() {
        try {
            return element();
        } catch (Exception e) {
            if (isErrorenabled())
            {
                error("RedisBlockingQueue.peek异常：" + e.getMessage());
            }
            return null;
        }
    }

    void assertNull(Object o) {
        if (o == null)
            throw new NullPointerException("不能为空！");
    }

    @Override
    public boolean offer(T e) {
        assertNull(e);
        try {
            return add(e);
        } catch (Exception e1) {
            if (isErrorenabled()) {
                error("RedisBlockingQueue.offer异常：" + e1.getMessage());
            }
            return false;
        }
    }

    @Override
    public void put(T e) throws InterruptedException {
        assertNull(e);
        setSocketSoTime(0);// 无时间限制
        add(e);
        setSocketSoTime(Protocol.DEFAULT_TIMEOUT);// 还原
    }

    /**
     * 注意timeout仅仅是socket SO_TIME层面，此方法自身不会有时延（即没有休眠等待）
     */
    @Override
    public boolean offer(T e, long timeout, TimeUnit unit) throws InterruptedException {
        assertNull(e);
        try {
            setSocketSoTime(unit.toMillis(timeout));
            return add(e);
        } catch (Exception e1) {
            if(isErrorenabled()){
                error("RedisBlockingQueue.offer异常：" + e1.getMessage());
            }
            return false;
        } finally {
            setSocketSoTime(Protocol.DEFAULT_TIMEOUT);
        }
    }

    @Override
    public T take() throws InterruptedException {
        setSocketSoTime(0);
        try {
            return deserialize(getJedis().blpop(0, redisKeyByteArray).get(1));
        } catch (Exception e) {
            if(isErrorenabled()){
                error("RedisBlockingQueue.offer异常：" + e.getMessage());
            }
            return null;
        } finally {
            setSocketSoTime(Protocol.DEFAULT_TIMEOUT);
        }
    }

    /**
     * 注意timeout仅仅是socket SO_TIME层面，此方法自身不会有时延（即没有休眠等待）
     */
    @Override
    public T poll(long timeout, TimeUnit unit) throws InterruptedException {
        setSocketSoTime(unit.toMillis(timeout));
        T t = poll();
        setSocketSoTime(Protocol.DEFAULT_TIMEOUT);
        return t;
    }

    @Override
    public int remainingCapacity() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int drainTo(Collection<? super T> c) {
        return drainTo(c, Integer.MAX_VALUE);
    }

    @Override
    public int drainTo(Collection<? super T> c, int maxElements) {
        if (c == null || maxElements <= 0)
            return 0;
        else {
            List<T> allListElementInRedis = subList(0, maxElements > size() ? size() : maxElements, true);
            c.addAll(allListElementInRedis);
            return allListElementInRedis.size();
        }
    }
}
