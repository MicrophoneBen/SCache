
package org.scache.cache.redis;

import org.scache.serialization.ISerializer;
import redis.clients.jedis.JedisPool;
import redis.clients.util.SafeEncoder;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * Redis {@link Set}实现<br>
 * <ul>
 * <li> {@link #iterator()}需要优化，全量查询会有内存溢出风险
 *
 * @author 君枫
 * @since 2014-8-18 下午4:16:19
 */
@SuppressWarnings("unchecked")
class RedisSet<T extends Serializable> extends AbstractRedisObject implements Set<T> {

    RedisSet(String name, JedisPool jedisPool, ISerializer serializer) {
        super(name, jedisPool, serializer);
    }

    @Override
    public int size() {
        return Long.valueOf(getJedis().scard(redisKeyByteArray)).intValue();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return getJedis().sismember(redisKeyByteArray, serialize((T) o));
    }

    Set<T> deserializeValue(Set<byte[]> byteList) {
        if (byteList != null && !byteList.isEmpty()) {
            Set<T> set = new HashSet<T>();
            for (byte[] bs : byteList) {
                try {
                    set.add((T) deserialize(bs));
                } catch (Exception e) {
                    // ignore
                }
            }
            return set;
        }
        return null;
    }

    Set<T> allElements() {
        return deserializeValue(getJedis().smembers(redisKeyByteArray));
    }

    @Override
    // TODO 待优化
    public Iterator<T> iterator() {
        return allElements().iterator();
    }

    @Override
    public Object[] toArray() {
        return allElements().toArray();
    }

    @Override
    public <V> V[] toArray(V[] a) {
        return allElements().toArray(a);
    }

    @Override
    public boolean add(T e) {
        return getJedis().sadd(redisKeyByteArray, serialize(e)) > 0;
    }

    @Override
    public boolean remove(Object o) {
        return getJedis().srem(redisKeyByteArray, serialize((T) o)) > 0;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o))
                return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean changed = false;
        if (c != null && !c.isEmpty()) {
            for (Object o : c) {
                if (add((T) o) && !changed) {
                    changed = true;
                }
            }
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        byte[] tempSetName = SafeEncoder.encode(SafeEncoder.encode(redisKeyByteArray) + ":tempSet[" + System.currentTimeMillis() + "]");
        // 添加到临时set中
        for (Object o : c) {
            getJedis().sadd(tempSetName, serialize((T) o));
        }
        long effectiveCount = getJedis().sinterstore(redisKeyByteArray, tempSetName, redisKeyByteArray);
        // 删除临时set
        getJedis().del(tempSetName);
        return effectiveCount >= 0;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = true;
        if (c != null && !c.isEmpty()) {
            for (Object o : c) {
                if (!remove(o))
                    flag = false;
            }
        }
        return flag;
    }

    @Override
    public void clear() {
        getJedis().del(redisKeyByteArray);
    }
}
