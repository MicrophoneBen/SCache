
package org.scache.cache.redis;

import org.scache.ILock;
import org.scache.IdGenerator;
import org.scache.cache.ISerializableCache;
import org.scache.serialization.ISerializer;
import org.scache.serialization.hessian.HessianSerializer;
import redis.clients.jedis.JedisPool;
import redis.clients.util.SafeEncoder;

import java.io.Serializable;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;


/**
 * Redis缓存：基于Jedis实现
 *
 * @author 君枫
 * @since 2014年3月11日 上午10:16:23
 */
@SuppressWarnings("unchecked")
class RedisCache extends AbstractRedisObject implements ISerializableCache {
    private final String nameSpace;
    private final ConcurrentMap<String, String> lockMap;


    RedisCache(JedisPool jedisPool, String nameSpace, ISerializer serializer) {
        super("RedisCache[" + System.currentTimeMillis() + "]", jedisPool, serializer);
        this.nameSpace = nameSpace + SEP;
        lockMap = getConcurrentMap("LockMap");
    }


    @Override
    public <K extends Serializable, V extends Serializable> ConcurrentMap<K, V> getConcurrentMap(String mapName) {
        RedisConcurrentMap<K, V> map = new RedisConcurrentMap<K, V>(getComponentFullName(ModuleType.CONCURRENT_MAP, mapName), jedisPool, serializer);
        return (ConcurrentMap<K, V>) Proxy.newProxyInstance(map.getClass().getClassLoader(),
                new Class<?>[]{ConcurrentMap.class}, map);
    }


    @Override
    public IdGenerator getIdGenerator(String idKey) {
        RedisIdGenerator idGenerator = new RedisIdGenerator(getComponentFullName(ModuleType.IDGENERATOR, idKey), jedisPool);
        return (IdGenerator) Proxy.newProxyInstance(idGenerator.getClass().getClassLoader(),
                new Class<?>[]{IdGenerator.class}, idGenerator);
    }


    @Override
    public <T extends Serializable> List<T> getList(String idKey) {
        RedisList<T> redisList = new RedisList<T>(getComponentFullName(ModuleType.LIST, idKey), jedisPool, serializer);
        return (List<T>) Proxy.newProxyInstance(redisList.getClass().getClassLoader(), new Class<?>[]{List.class}, redisList);
    }


    @Override
    public <T extends Serializable> BlockingQueue<T> getBlockingQueue(String queueName) {
        RedisBlockingQueue<T> redisBlockingQueue =
                new RedisBlockingQueue<T>(getComponentFullName(ModuleType.BLOCKING_QUEUE, queueName), jedisPool, serializer);
        return (BlockingQueue<T>) Proxy.newProxyInstance(redisBlockingQueue.getClass().getClassLoader(), new Class<?>[]{
                BlockingQueue.class, List.class}, redisBlockingQueue);
    }


    @Override
    public <T extends Serializable> Set<T> getConcurrentSet(String setName) {
        RedisSet<T> redisSet = new RedisSet<T>(getComponentFullName(ModuleType.CONCURRENT_SET, setName), jedisPool, serializer);
        return (Set<T>) Proxy.newProxyInstance(redisSet.getClass().getClassLoader(), new Class<?>[]{Set.class}, redisSet);
    }

    @Override
    public ILock getLock(String lockName) {
        //锁1小时有效
        expireComponent(ModuleType.LOCK, lockName, TimeUnit.SECONDS, 3600);
        RedisLock redisLock = new RedisLock(getComponentFullName(ModuleType.LOCK, lockName), jedisPool, serializer);
        return (ILock) Proxy.newProxyInstance(redisLock.getClass().getClassLoader(), new Class<?>[]{ILock.class}, redisLock);
    }

    @Override
    public boolean removeComponent(ModuleType type, String componentName) {
        String name = getComponentFullName(type, componentName);
        return getJedis().del(name) > 0;
    }

    @Override
    public boolean expireComponent(ModuleType type, String componentName, TimeUnit unit, long time) {
        String name = getComponentFullName(type, componentName);
        return getJedis().expire(name, Long.valueOf(unit.toSeconds(time)).intValue()) > 0;
    }


    String getComponentFullName(ModuleType type, String componentName) {
        return nameSpace + type.getModuleName() + SEP + componentName;
    }


    public static void main(String[] args) {
        /*
         * System.out.println(Serializable.class.isAssignableFrom(byte[].class));
		 * System.out.println(new StringBuilder() instanceof CharSequence);
		 * System.out.println(new StringBuffer() instanceof CharSequence);
		 * System.out.println(new String() instanceof CharSequence);
		 */
        System.out.println(canLookAsString(new Integer("1")));
        ISerializer s = new HessianSerializer();
        byte[] b = s.serialize(1);
        for (int i = 0; i < b.length; i++) {
            System.out.println((char) b[i]);
        }
        System.out.println("1111111111111111111");
        b = SafeEncoder.encode("1");
        for (int i = 0; i < b.length; i++) {
            System.out.println((char) b[i]);
        }
        s.deserialize(b);
    }
}
