package org.scache.cache.redis;

import org.scache.ILock;
import org.scache.serialization.ISerializer;
import redis.clients.jedis.JedisPool;

import java.util.UUID;

/**
 * 基于Redis分布式锁，使用Redis String实现
 *
 * @author junfeng
 * @since 15/7/9 13:38
 */
class RedisLock extends AbstractRedisObject implements ILock {
    static final String appID = UUID.randomUUID().toString();


    public RedisLock(String redisKey, JedisPool jedisPool, ISerializer serializer) {
        super(redisKey, jedisPool, serializer);
    }


    String getLockerInfo() {
        return appID + ":" + Thread.currentThread().getName();
    }


    @Override
    public boolean tryLock() {
        String lockerInfo = getJedis().get(redisKey);
        String wantLockerInfo = getLockerInfo();
        if (!wantLockerInfo.equals(lockerInfo)) {
            return getJedis().setnx(redisKey, wantLockerInfo) > 0;
        } else {
            return true;
        }
    }

    @Override
    public void unlock() {
        String lockerInfo = getJedis().get(redisKey);
        String wantLockerInfo = getLockerInfo();
        if (wantLockerInfo.equals(lockerInfo)) {
            getJedis().del(redisKey);
        }
    }
}
