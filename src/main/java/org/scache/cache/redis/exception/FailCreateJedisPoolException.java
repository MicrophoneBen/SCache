package org.scache.cache.redis.exception;

/**
 * 构建JedisPool失败
 * @author junfeng
 * @since 15/7/7 16:05
 */
public class FailCreateJedisPoolException extends RuntimeException {
    public FailCreateJedisPoolException() {
    }

    public FailCreateJedisPoolException(String message) {
        super(message);
    }

    public FailCreateJedisPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailCreateJedisPoolException(Throwable cause) {
        super(cause);
    }
}
