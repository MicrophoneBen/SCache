
package org.scache.cache.redis.test;

import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.scache.cache.ISerializableCache;
import org.scache.cache.redis.test.RedisConcurrentMapTest.Person;


/**
 * RedisBlockingQueue测试
 * 
 * @author 君枫
 * @version 1.0
 * @since 2014-8-22 下午2:46:22
 * @category com.ailk.cache.redis.test
 */
public class RedisBlockingQueueTest
{

	static Random r = new Random();
	static ISerializableCache cache = RedisConcurrentMapTest.getLocalCache();
	static BlockingQueue<Person> blockingQueue = cache.getBlockingQueue("personQueue");

	public static void main(String[] args) throws Exception
	{
		for (int i = 0; i < 10; i++)
		{
			test_add();
		}
		test_remove();
		test_check();
	}

	static Person newPerson()
	{
		return new Person("Person" + r.nextInt(100), r.nextInt(100));
	}

	/**
	 * 测试add、offer、put
	 */
	static void test_add() throws Exception
	{
		System.out.println("#test_add ----------------------");
		System.out.println("test add :" + blockingQueue.add(newPerson()));
		System.out.println("test offer :" + blockingQueue.offer(newPerson()));
		System.out.println("test offer with timeout :" + blockingQueue.offer(newPerson(), 10, TimeUnit.SECONDS));
		blockingQueue.put(newPerson());
	}

	/**
	 * 测试remove、poll
	 * 
	 * @throws Exception
	 */
	static void test_remove() throws Exception
	{
		System.out.println("#test_remove ----------------------");
		System.out.println(blockingQueue.remove());
		System.out.println(blockingQueue.remove(newPerson()));
		System.out.println(blockingQueue.poll());
		System.out.println(blockingQueue.poll(10, TimeUnit.SECONDS));
		System.out.println(blockingQueue.take());
		System.out.println("RedisQueue剩余个数：" + blockingQueue.size());
		// List<Person> list = new ArrayList<RedisCacheTest.Person>();
		List<Person> list = cache.getList("personList");
		System.out.println("拷贝到List的个数：" + blockingQueue.drainTo(list, 10) + ",list:" + list);
	}

	/**
	 * 测试peek、element
	 */
	static void test_check()
	{
		System.out.println("#test_check ----------------------");
		System.out.println("before peek  size :" + blockingQueue.size());
		System.out.println(blockingQueue.peek());
		System.out.println("after peek  size :" + blockingQueue.size());
		System.out.println(blockingQueue.element());
		System.out.println("after element  size :" + blockingQueue.size());
	}
}
