package org.scache.cache.redis.test;

import org.scache.ILock;
import org.scache.cache.ISerializableCache;

/**
 * @author junfeng
 * @since 15/7/9 14:02
 */
public class RedisLockTest {
    public static void main(String[] args) {
        ISerializableCache cache = RedisConcurrentMapTest.getLocalCache();
        String id="Locker_"+System.currentTimeMillis();
        ILock lock = cache.getLock("testLock");
        if (lock.tryLock()) {
            System.out.println("["+id+"]√获得到锁");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                System.out.println("["+id+"]√释放锁");
            }
        } else {
            System.out.println("["+id+"]×获取不到锁");
        }
    }
}
