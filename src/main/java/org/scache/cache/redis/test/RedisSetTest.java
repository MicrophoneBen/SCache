
package org.scache.cache.redis.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.scache.cache.ISerializableCache;
import org.scache.cache.redis.test.RedisConcurrentMapTest.Person;


/**
 * RedisSet测试
 * 
 * @author 君枫
 * @version 1.0
 * @since 2014-8-25 上午9:20:24
 * @category com.ailk.cache.redis.test
 */
public class RedisSetTest
{

	static Random r = new Random();
	static ISerializableCache cache = RedisConcurrentMapTest.getLocalCache();
	static Set<Person> set = cache.getConcurrentSet("personSet");

	static Person newPerson()
	{
		return new Person("Person" + r.nextInt(100), r.nextInt(100));
	}

	public static void main(String[] args) throws Exception
	{
		// test_add();
		// test_remove();
		test_check();
	}

	static void test_check()
	{
		System.out.println("#test_check ----------------------");
		List<Person> ps = new ArrayList<RedisConcurrentMapTest.Person>();
		for (int i = 0; i < 1; i++)
		{
			ps.add(newPerson());
		}
		System.out.println("contains all :" + set.containsAll(ps));
		System.out.println(set.contains(newPerson()));
		System.out.println("set is empty :" + set.isEmpty());
	}

	static void test_add() throws Exception
	{
		System.out.println("#test_add ----------------------");
		Person p = newPerson();
		System.out.println("add new person：" + p + set.add(p));// true
		System.out.println("add new person：" + p + set.add(p));// false
		List<Person> ps = new ArrayList<RedisConcurrentMapTest.Person>();
		for (int i = 0; i < 2200; i++)
		{
			p = newPerson();
			// System.out.println("add new person：" + p +
			// set.add(p));//如果放开，下面addall则返回false
			ps.add(p);
		}
		System.out.println("add all person：" + set.addAll(ps));
		System.out.println("current set size:" + set.size());
	}

	static void test_remove() throws Exception
	{
		System.out.println("#test_remove ----------------------");
		System.out.println("current set size:" + set.size());
		Person p = newPerson();
		System.out.println("remove " + p + set.remove(p));
		System.out.println("remove " + p + set.remove(p));
		List<Person> ps = new ArrayList<RedisConcurrentMapTest.Person>();
		for (int i = 0; i < 200; i++)
		{
			p = newPerson();
			ps.add(p);
		}
		System.out.println("remove all person：" + set.removeAll(ps));
		System.out.println("current set size:" + set.size());
		// retain all
		ps.clear();
		for (int i = 0; i < 200; i++)
		{
			p = newPerson();
			ps.add(p);
		}
		// System.out.println("保留的元素：" + ps);
		System.out.println("retain all :" + set.retainAll(ps));
		System.out.println("current set size:" + set.size());
		set.clear();
		System.out.println("current set size:" + set.size());
	}
}
