
package org.scache.cache.redis.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.scache.cache.ISerializableCache;
import org.scache.cache.redis.test.RedisConcurrentMapTest.Person;


/**
 * 测试 RedisListTest
 * 
 * @author 君枫
 * @version 1.0
 * @since 2014-8-22 下午2:41:07
 * @category com.ailk.cache.redis.test
 */
public class RedisListTest
{

	static Random r = new Random();

	public static void main(String[] args)
	{
		ISerializableCache cache = RedisConcurrentMapTest.getLocalCache();
		test_list_add(cache);
		test_list_get(cache);
		test_list_remove(cache);
	}

	static void test_list_add(ISerializableCache cache)
	{
		List<Person> ps = cache.getList("personList");
		Person p;
		// -------------- test add from index
		int index = 0;
		ps.add(index, new Person("Person" + r.nextInt(100), r.nextInt(100)));
		// -------------- test add
		// for (int i = 0; i < 100; i++)
		// {
		// p = new Person("Person" + r.nextInt(100), r.nextInt(100));
		// System.out.println(p + "添加成功：" + ps.add(p));
		// }
		// -------------- test add all
		List<Person> persons = new ArrayList<RedisConcurrentMapTest.Person>();
		for (int i = 0; i < 10; i++)
		{
			p = new Person("Person" + r.nextInt(100), r.nextInt(100));
			persons.add(p);
			System.out.println(p);
		}
		// System.out.println("add all :" + ps.addAll(persons));
		index = 1;
		System.out.println("add all from index " + index + ps.addAll(index, persons));
	}

	static void test_list_get(ISerializableCache cache)
	{
		List<Person> ps = cache.getList("personList");
		System.out.println(ps.size());
		System.out.println(ps.isEmpty());
		Person p = new Person("Person" + 48, 69);
		System.out.println(ps.contains(p));// 写一个在库里面存在的
		System.out.println(ps.get(ps.indexOf(p)));
		for (int i = 0; i < 20; i++)
		{
			System.out.println(i + "-->" + ps.get(i));
		}
	}

	static void test_list_remove(ISerializableCache cache)
	{
		// System.out.println(cache.removeList("personList"));
		List<Person> ps = cache.getList("personList");
		System.out.println(ps.remove(new Person("Person48", 85)));
		List<Person> persons = new ArrayList<RedisConcurrentMapTest.Person>();
		persons.add(new Person("Person2", 12));
		persons.add(new Person("Person56", 30));
		persons.add(new Person("Person62", 10));
		persons.add(new Person("Person83", 8));
		persons.add(new Person("Person54", 85));
		System.out.println(ps.removeAll(persons));
		for (int i = 0; i < 10; i++)
		{
			// System.out.println(ps.remove(r.nextInt(140)));
			System.out.println(ps.remove(0));
		}
	}
}
