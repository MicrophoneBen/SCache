
package org.scache.cache.redis.test;

import org.scache.IdGenerator;
import org.scache.cache.CacheNameSpace;
import org.scache.cache.ISerializableCache;
import org.scache.cache.redis.RedisCacheProvider;
import org.scache.serialization.hessian.Hessian2Serializer;
import redis.clients.jedis.JedisPoolConfig;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;


/**
 * {@link RedisCacheProvider} & RedisCache 测试
 *
 * @author 君枫
 * @since 2014年3月13日 上午9:12:52
 */
public class RedisConcurrentMapTest {

    public static void main(String[] args) throws Exception {
        ISerializableCache serializableCache = getLocalCache();
        ConcurrentMap cache = serializableCache.getConcurrentMap("testMap");
        // ISerializableCache<String, String> cache = getRemoteCache();
        //  test_set(cache);
        // test_get(cache);
        // test_containsKey(cache);System.out.println(cache.size());
        // test_putIfAbsent(cache);
        //test_remove(cache);
        //test_replace(cache);
        //test_set(cache);
        //test_lock(cache);
        //test_idGenerator(serializableCache.getIdGenerator("test_idGenerator"));
    }

    static void test_idGenerator(IdGenerator idGenerator) throws Exception {
        for (int i = 0; i < 100; i++) {
            idGenerator.generateId(true);
        }
        System.out.println(idGenerator.getCurrentId());
    }

    public static ISerializableCache getLocalCache() {
        RedisCacheProvider cacheProvider = new RedisCacheProvider("localhost", 6379, new JedisPoolConfig());
        cacheProvider.setSerializer(new Hessian2Serializer());
        cacheProvider.init();
        return cacheProvider.getCache(CacheNameSpace.CM);
    }

    private static void test_set(ConcurrentMap cache) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("1", "2");
        map.put("11", "x2");
        map.put("12", "x2");

        cache.put("map", map);
        cache.putAll(map);
        cache.put("12", "12xxxx");
        cache.put("11", "11fdafdsafdsa");
        cache.put("redisKeyByteArray", "junfeng");
        System.out.println("set over");
    }

    private static void test_get(ConcurrentMap cache) {
        System.out.println(cache.get("map"));
        System.out.println(cache.get("1"));
        System.out.println(cache.get("PerformanceManager:snaps:22.13.504.0.265.2.76.0"));
        System.out.println(cache.get("12"));
    }

    private static void test_containsKey(ConcurrentMap cache) {
        System.out.println(cache.containsKey("redisKeyByteArray"));
        System.out.println(cache.containsKey("map"));
        System.out.println(cache.containsKey("1"));
    }

    private static void test_putIfAbsent(ConcurrentMap cache) {
        String key = "redisKeyByteArray";
        System.out.println(cache.containsKey(key));
        cache.putIfAbsent(key, "jufeng");
        System.out.println(cache.get(key));
        key = "名称";
        System.out.println(cache.containsKey(key));
        cache.putIfAbsent(key, "wangyj9");
        System.out.println(cache.get(key));
    }

    private static void test_remove(ConcurrentMap cache) {
        String key = "redisKeyByteArray";
        System.out.println("存在key=" + key + "：" + cache.containsKey(key));
        System.out.println("删除key=" + key + "：" + cache.remove(key));
        key = "名称";
        String value = "jufeng";
        System.out.println("存在key=" + key + "：" + cache.containsKey(key) + "值为：" + cache.get(key));
        System.out.println("value为'" + value + "'时;删除" + key + "映射：" + cache.remove(key, value));
    }

    private static void test_replace(ConcurrentMap cache) {
        String key = "redisKeyByteArray";
        String oldValue = "junfeng";
        String newValue = "君枫";
        System.out.println("设置键值对" + key + "=" + oldValue + ":" + cache.put(key, oldValue));
        System.out.println("替换值为" + newValue + ":" + cache.replace(key, oldValue, newValue));
        System.out.println("当前键" + key + "对应的值为：" + cache.get(key));
    }

    private static void test_lock(final ConcurrentMap cache) throws Exception {
        int count = 1200;
        final CountDownLatch countDownLatch = new CountDownLatch(count);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(10, 10, 60L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>()) {

            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                countDownLatch.countDown();
            }
        };
        for (int i = 0; i < count; i++) {
            threadPoolExecutor.submit(new Runnable() {

                void test_put() {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("1a" + Thread.currentThread().getName(), "2");
                    map.put("1b" + Thread.currentThread().getName(), "2");
                    map.put("1c" + Thread.currentThread().getName(), "2");
                    map.put("1d" + Thread.currentThread().getName(), "2");
                    cache.putAll(map);
                    String key = "锁" + new Random().nextInt(1200);
                    boolean locked = cache.putIfAbsent(key, "2") == null;
                    if (locked) {
                        System.out.println(Thread.currentThread().getName() + " hold lock:" + key);
                        //cache.expire(key, 60);
                    } else {
                        System.out.println("【!】" + Thread.currentThread().getName() + " can't hold lock:" + key);
                    }
                }

                @Override
                public void run() {
                    try {
                        test_put();
                    } catch (Exception e) {
                        e = new Exception(Thread.currentThread().getName(), e);
                        e.printStackTrace();
                    }
                }
            });
        }
        countDownLatch.await();
        threadPoolExecutor.shutdown();
    }

    public static class Person implements Serializable {

        private static final long serialVersionUID = -6882591538920979495L;
        public String name;
        public int age;

        public Person() {
        }

        public Person(String name, int age) {
            super();
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return "Person [redisKeyByteArray=" + name + ", age=" + age + "]";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + age;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Person other = (Person) obj;
            if (age != other.age)
                return false;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            return true;
        }
    }
}
