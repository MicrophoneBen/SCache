package org.scache.cache.redis;

import org.scache.serialization.ISerializer;
import org.scache.util.CollectionUtil;
import redis.clients.jedis.JedisPool;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentMap;

/**
 * ConcurrentMap的Redis实现
 *
 * @author junfeng
 * @since 15/7/8 14:20
 */
class RedisConcurrentMap<K extends Serializable, V extends Serializable> extends AbstractRedisObject
        implements ConcurrentMap<K, V> {

    public RedisConcurrentMap(String name, JedisPool jedisPool, ISerializer serializer) {
        super(name, jedisPool, serializer);
    }


    @Override
    public V putIfAbsent(K key, V value) {
        byte[] keyBytes = serialize(key);
        byte[] valueBytes = serialize(value);
        if (getJedis().hsetnx(redisKeyByteArray, serialize(key), valueBytes) == 0) {
            return deserialize(getJedis().hget(redisKeyByteArray, keyBytes));
        } else {
            return null;
        }
    }

    @Override
    public boolean remove(Object key, Object value) {
        checkInvalid(key);
        checkInvalid(value);
        byte[] keyBytes = serialize((Serializable) key);
        V oldValue = deserialize(getJedis().hget(redisKeyByteArray, keyBytes));
        if (value.equals(oldValue)) {
            return getJedis().hdel(redisKeyByteArray, keyBytes) > 0;
        }
        return false;
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        checkInvalid(oldValue);
        byte[] keyBytes = serialize(key);
        V oldValueInRedis = deserialize(getJedis().hget(redisKeyByteArray, keyBytes));
        if (oldValue.equals(oldValueInRedis)) {
            byte[] newValueBytes = serialize(newValue);
            return getJedis().hset(redisKeyByteArray, keyBytes, newValueBytes) >= 0;
        }
        return false;
    }

    @Override
    public V replace(K key, V value) {
        byte[] keyBytes = serialize(key);
        if (getJedis().hexists(redisKeyByteArray, keyBytes)) {
            byte[] valueBytes = serialize(value);
            getJedis().hset(redisKeyByteArray, keyBytes, valueBytes);
        }
        return null;
    }

    @Override
    public int size() {
        return getJedis().hlen(redisKeyByteArray).intValue();
    }

    @Override
    public boolean isEmpty() {
        return size() <= 0;
    }

    @Override
    public boolean containsKey(Object key) {
        checkInvalid(key);
        return getJedis().hexists(redisKeyByteArray, serialize((Serializable) key));
    }

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException("do operate 'containsValue' in redis");
    }

    @Override
    public V get(Object key) {
        checkInvalid(key);
        return deserialize(getJedis().hget(redisKeyByteArray, serialize((Serializable) key)));
    }

    @Override
    public V put(K key, V value) {
        byte[] keyBytes = serialize(key);
        byte[] valueBytes = serialize(value);
        byte[] bs=getJedis().hget(redisKeyByteArray, keyBytes);
        V oldValue = deserialize(bs);
        getJedis().hset(redisKeyByteArray, keyBytes, valueBytes);
        return oldValue;
    }

    @Override
    public V remove(Object key) {
        checkInvalid(key);
        byte[] bs = serialize((Serializable) key);
        V oldValue = deserialize(getJedis().hget(redisKeyByteArray, bs));
        getJedis().hdel(redisKeyByteArray, bs);
        return oldValue;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        Map<K, V> map = (Map<K, V>) m;
        Map.Entry<K, V> entry;
        for (Iterator<Entry<K, V>> it = map.entrySet().iterator(); it.hasNext(); ) {
            entry = it.next();
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<K> keySet() {
        Set<byte[]> keySetBytes = getJedis().hkeys(redisKeyByteArray);
        if (CollectionUtil.isNotNull(keySetBytes)) {
            Set<K> keySet = new HashSet<K>(keySetBytes.size());
            for (byte[] bs : keySetBytes) {
                keySet.add((K) deserialize(bs));
            }
            return keySet;
        } else {
            return Collections.emptySet();
        }
    }

    @Override
    public Collection<V> values() {
        List<byte[]> valueBytes = getJedis().hvals(redisKeyByteArray);
        if (CollectionUtil.isNotNull(valueBytes)) {
            List<V> values = new ArrayList<V>(valueBytes.size());
            for (byte[] bs : valueBytes) {
                values.add((V) deserialize(bs));
            }
            return values;
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        throw new UnsupportedOperationException();
    }
}
