
package org.scache.cache.redis;

import org.scache.serialization.ISerializer;
import redis.clients.jedis.BinaryClient.LIST_POSITION;
import redis.clients.jedis.JedisPool;

import java.io.Serializable;
import java.util.*;


/**
 * Redis的 {@link List}实现
 *
 * @author 君枫
 * @since 2014-8-15 下午2:21:06
 */
@SuppressWarnings("unchecked")
class RedisList<T extends Serializable> extends AbstractRedisObject implements List<T> {

    private static final int batchSize = 50;

    RedisList(String listName, JedisPool jedisPool, ISerializer serializer) {
        super(listName, jedisPool, serializer);
    }

    @Override
    public int size() {
        return getJedis().llen(redisKeyByteArray).intValue();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public Iterator<T> iterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<T> listIterator(final int ind) {
        return new ListIterator<T>() {

            private int currentIndex = ind - 1;
            private boolean removeExecuted;

            @Override
            public boolean hasNext() {
                int size = size();
                return currentIndex + 1 < size && size > 0;
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException("No such element at index " + currentIndex);
                }
                currentIndex++;
                removeExecuted = false;
                return RedisList.this.get(currentIndex);
            }

            @Override
            public void remove() {
                if (removeExecuted) {
                    throw new IllegalStateException("Element been already deleted");
                }
                RedisList.this.remove(currentIndex);
                currentIndex--;
                removeExecuted = true;
            }

            @Override
            public boolean hasPrevious() {
                int size = size();
                return currentIndex - 1 < size && size > 0 && currentIndex >= 0;
            }

            @Override
            public T previous() {
                if (!hasPrevious()) {
                    throw new NoSuchElementException("No such element at index " + currentIndex);
                }
                removeExecuted = false;
                T res = RedisList.this.get(currentIndex);
                currentIndex--;
                return res;
            }

            @Override
            public int nextIndex() {
                return currentIndex + 1;
            }

            @Override
            public int previousIndex() {
                return currentIndex;
            }

            @Override
            public void set(T e) {
                if (currentIndex >= size() - 1) {
                    throw new IllegalStateException();
                }
                RedisList.this.set(currentIndex, e);
            }

            @Override
            public void add(T e) {
                RedisList.this.add(currentIndex + 1, e);
                currentIndex++;
            }
        };
    }

    @Override
    public Object[] toArray() {
        List<T> list = subList(0, size());
        return list.toArray();
    }

    @Override
    public <V> V[] toArray(V[] a) {
        List<T> list = subList(0, size());
        return list.toArray(a);
    }

    @Override
    public boolean add(T e) {
        return getJedis().rpush(redisKeyByteArray, serialize(e)) > 0;
    }

    @Override
    public boolean remove(Object o) {
        return remove(o, 1);
    }

    protected boolean remove(final Object o, final int count) {
        return getJedis().lrem(redisKeyByteArray, count, serialize((T) o)) > 0;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (isEmpty()) {
            return false;
        }
        Collection<Object> copy = new ArrayList<Object>(c);
        int to = div(size(), batchSize);
        for (int i = 0; i < to; i++) {
            List<byte[]> byteList = getJedis().lrange(redisKeyByteArray, i * batchSize, i * batchSize + batchSize - 1);
            List<T> list = deserializeValue(byteList);
            for (Iterator<Object> iterator = copy.iterator(); iterator.hasNext(); ) {
                Object obj = iterator.next();
                int index = list.indexOf(obj);
                if (index != -1) {
                    iterator.remove();
                }
            }
        }
        return copy.isEmpty();
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return getJedis().rpush(redisKeyByteArray, serializeValue((List<T>) c)) >= c.size();
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (index > 0) {
            byte[] lastElemnt = getByteArray(index - 1);
            byte[] currentElemnt;
            for (T v : c) {
                currentElemnt = serialize(v);
                getJedis().linsert(redisKeyByteArray, LIST_POSITION.AFTER, lastElemnt, currentElemnt);
                lastElemnt = currentElemnt;
            }
        } else if (index == 0) {
            Collections.reverse((List<?>) c);
            byte[][] bytes = serializeValue((Collection<T>) c);
            getJedis().lpush(redisKeyByteArray, bytes);
        } else
            return false;
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = true;
        for (Object o : c) {
            if (!remove(o))
                flag = false;
        }
        return flag;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        for (Iterator<T> iterator = iterator(); iterator.hasNext(); ) {
            T object = iterator.next();
            if (!c.contains(object)) {
                iterator.remove();
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public void clear() {
        getJedis().del(redisKeyByteArray);
    }

    @Override
    public T get(int index) {
        return deserialize(getJedis().lindex(redisKeyByteArray, index));
    }

    protected byte[] getByteArray(int index) {
        checkIndex(index, size());
        return getJedis().lindex(redisKeyByteArray, index);
    }

    @Override
    public T set(int index, T element) {
        checkIndex(index, size());
        T old = get(index);
        getJedis().lset(redisKeyByteArray, index, serialize(element));
        return old;
    }

    @Override
    public void add(int index, T element) {
        if (index > 0) {
            getJedis().linsert(redisKeyByteArray, LIST_POSITION.AFTER, getByteArray(index - 1), serialize(element));
        } else if (index == 0) {
            getJedis().lpush(redisKeyByteArray, serialize(element));
        }
    }

    private void checkIndex(int index, int size) {
        if (!isInRange(index, size))
            throw new IndexOutOfBoundsException("index: " + index + " but current size: " + size);
    }

    private boolean isInRange(int index, int size) {
        return index >= 0 && index < size;
    }

    T removeFromLeft(int index, int size) {
        List<byte[]> byteList = getJedis().lrange(redisKeyByteArray, 0, index);
        getJedis().ltrim(redisKeyByteArray, index + 1, size - 1);// 只保留另一半
        T removed = deserialize(byteList.remove(index));
        Collections.reverse(byteList);
        byte[][] reversedBytes = new byte[byteList.size()][];
        byteList.toArray(reversedBytes);
        getJedis().lpush(redisKeyByteArray, reversedBytes);
        return removed;
    }

    T removeFromRight(int index, int size) {
        List<byte[]> byteList = getJedis().lrange(redisKeyByteArray, index, size - 1);
        getJedis().ltrim(redisKeyByteArray, 0, index - 1);// 只保留另一半
        T removed = deserialize(byteList.remove(0));
        byte[][] bytes = new byte[byteList.size()][];
        byteList.toArray(bytes);
        getJedis().rpush(redisKeyByteArray, bytes);
        return removed;
    }

    @Override
    public T remove(int index) {
        int size = size();
        checkIndex(index, size);
        if (index == 0) {
            return deserialize(getJedis().lpop(redisKeyByteArray));
        } else {
            if (index <= (size / 2))// 处在左半部分
            {
                return removeFromLeft(index, size);
            } else {
                return removeFromRight(index, size);
            }
        }
    }

    private int div(int p, int q) {
        int div = p / q;
        int rem = p - q * div; // equal to p % q
        if (rem == 0) {
            return div;
        }
        return div + 1;
    }

    byte[][] serializeValue(Collection<T> list) {
        byte[][] bytes = null;
        if (list != null && !list.isEmpty()) {
            bytes = new byte[list.size()][];
            int i = 0;
            for (Iterator<T> it = list.iterator(); it.hasNext(); ) {
                bytes[i++] = serialize(it.next());
            }
        }
        return bytes;
    }

    List<T> deserializeValue(List<byte[]> byteList) {
        if (byteList != null && !byteList.isEmpty()) {
            List<T> list = new ArrayList<T>(byteList.size());
            for (byte[] bs : byteList) {
                try {
                    list.add((T) deserialize(bs));
                } catch (Exception e) {
                    // ignore
                }
            }
            return list;
        }
        return null;
    }

    @Override
    public int indexOf(Object o) {
        if (isEmpty()) {
            return -1;
        }
        int to = div(size(), batchSize);
        for (int i = 0; i < to; i++) {
            List<byte[]> byteList = getJedis().lrange(redisKeyByteArray, i * batchSize, i * batchSize + batchSize - 1);
            List<T> list = deserializeValue(byteList);
            int index = list.indexOf(o);
            if (index != -1) {
                return index + i * batchSize;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        if (isEmpty()) {
            return -1;
        }
        final int size = size();
        int to = div(size, batchSize);
        for (int i = 1; i <= to; i++) {
            final int j = i;
            final int startIndex = -i * batchSize;
            List<byte[]> byteList = getJedis().lrange(redisKeyByteArray, startIndex, size - (j - 1) * batchSize);
            List<T> list = deserializeValue(byteList);
            int index = list.lastIndexOf(o);
            if (index != -1) {
                return Math.max(size + startIndex, 0) + index;
            }
        }
        return -1;
    }

    @Override
    public ListIterator<T> listIterator() {
        return listIterator(0);
    }

    @Override
    public List<T> subList(final int fromIndex, final int toIndex) {
        return subList(fromIndex, toIndex, false);
    }

    /**
     * @param fromIndex
     * @param toIndex
     * @param deletesubedElement 删除subList内元素
     * @return
     */
    protected List<T> subList(final int fromIndex, final int toIndex, boolean deletesubedElement) {
        int size = size();
        if (fromIndex < 0 || toIndex > size) {
            throw new IndexOutOfBoundsException("fromIndex: " + fromIndex + " toIndex: " + toIndex + " size: " + size);
        }
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException("fromIndex: " + fromIndex + " toIndex: " + toIndex);
        }
        List<byte[]> byteList = getJedis().lrange(redisKeyByteArray, fromIndex, toIndex - 1);
        List<T> subList = deserializeValue(byteList);
        if (deletesubedElement)
            removeAll(subList);
        return subList;
    }
}
