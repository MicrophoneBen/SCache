
package org.scache.cache.redis;

import org.scache.cache.AbstractSerializableCacheProvider;
import org.scache.cache.ISerializableCache;
import org.scache.cache.redis.exception.FailCreateJedisPoolException;
import org.scache.serialization.ISerializer;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.lang.reflect.Proxy;


/**
 * Redis缓存提供者
 *
 * @author 君枫
 * @since 2014年3月11日 下午2:18:56
 */
public class RedisCacheProvider extends AbstractSerializableCacheProvider {

    private final long id = System.currentTimeMillis();
    /**
     * jedis
     */
    private String host;
    private int port;
    private JedisPool jedisPool;
    private JedisPoolConfig jedisPoolConfig;

    @Override
    public void init() {
        if (jedisPoolConfig != null && host != null && !host.trim().isEmpty() && port > 0) {
            jedisPool = new JedisPool(jedisPoolConfig, host, port);
        } else {
            throw new FailCreateJedisPoolException("host:" + host + ",port:" + port);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        if (jedisPool != null) {
            jedisPool.destroy();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ISerializableCache buildCache(String namespace, ISerializer serializer) {
        RedisCache cache = new RedisCache(jedisPool, namespace, serializer);
        return (ISerializableCache) Proxy.newProxyInstance(cache.getClass().getClassLoader(),
                new Class[]{ISerializableCache.class}, cache);
    }


    @Override
    public String getName() {
        return "RedisCacheProvider[" + host + ":" + port + "][" + id + "]";
    }

    public RedisCacheProvider(String host, int port, JedisPoolConfig jedisPoolConfig) {
        this.port = port;
        this.host = host;
        this.jedisPoolConfig = jedisPoolConfig;
    }

}
