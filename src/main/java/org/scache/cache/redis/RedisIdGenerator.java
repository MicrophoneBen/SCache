
package org.scache.cache.redis;

import org.scache.IdGenerator;
import redis.clients.jedis.JedisPool;


/**
 * 基于reids实现的ID生成器
 *
 * @author 君枫
 * @since 2014-4-27 下午4:01:47
 */
class RedisIdGenerator extends AbstractRedisObject implements IdGenerator {

    private final String idKey;

    RedisIdGenerator(String idKey, JedisPool jedisPool) {
        super(idKey, jedisPool, null);
        this.idKey = idKey;
    }

    @Override
    public String getCurrentId() throws Exception {
        return getJedis().get(idKey);
    }

    @Override
    public String generateId(boolean updateCurrentId) throws Exception {
        if (updateCurrentId) {
            return String.valueOf(getJedis().incr(idKey));
        }
        long currentId = Long.valueOf(getCurrentId());
        currentId++;
        return String.valueOf(currentId);
    }

    @Override
    public boolean updateCurrentId(String id) throws Exception {
        return "ok".equals(getJedis().set(idKey, id));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.linkage.toptea.util.IdGenerator#available()
     */
    @Override
    public boolean available() {
        try {
            getCurrentId();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
