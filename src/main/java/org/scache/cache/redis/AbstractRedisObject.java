
package org.scache.cache.redis;

import org.scache.serialization.ISerializer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.util.SafeEncoder;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.security.InvalidParameterException;

import static org.scache.util.ClassUtil.isPrimitiveOrWrapper;


/**
 * 抽象的Redis对象
 * <ul>
 * <li>通过 {@link ThreadLocal}实现<b>线程安全</b></li>
 * <li>通过动态代理实现所有jedis操作不需要显式的释放资源</li>
 * <li>key如果可以当String类型({@link #canLookAsString(Object)})处理则不采用 {@link #serializer}
 * 序列化、反序列化，而是采用 {@link String#getBytes(String)} 方法</li>
 * <li>value统一采用{@link #serializer}序列化和反序列化</li>
 * </ul>
 *
 * @author 君枫
 * @since 2014-8-15 下午3:10:50
 */
abstract class AbstractRedisObject implements InvocationHandler {

    protected static final ThreadLocal<Jedis> jedisThreadLocal = new ThreadLocal<Jedis>();
    protected JedisPool jedisPool;// 线程安全
    protected ISerializer serializer;// 线程安全
    protected byte[] redisKeyByteArray;
    protected String redisKey;

    public AbstractRedisObject(String redisKey, JedisPool jedisPool, ISerializer serializer) {
        this.jedisPool = jedisPool;
        this.serializer = serializer;
        this.redisKeyByteArray = SafeEncoder.encode(redisKey);
        this.redisKey = redisKey;
    }

    void checkInvalid(Object o) {
        if (o == null || !(o instanceof Serializable)) {
            throw new InvalidParameterException("object[" + o + "] must not be null and implements Serializable");
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Jedis jedis = jedisPool.getResource();
        jedisThreadLocal.set(jedis);
        try {
            Object result = method.invoke(this, args);
            return result;
        } catch (Exception e) {
            throw e;
        } finally {
            jedis.close();
        }
    }

    protected Jedis getJedis() {
        return jedisThreadLocal.get();
    }


    /**
     * 设置socket读取超时时间
     *
     * @param soTime
     */
    protected void setSocketSoTime(long soTime) {
        getJedis().getClient().setSoTimeout(Long.valueOf(soTime).intValue());
    }

    /**
     * 可以将此对象看做String
     */
    protected static boolean canLookAsString(Object o) {
        return o instanceof CharSequence || o instanceof Number || isPrimitiveOrWrapper(o.getClass());
    }


    /**
     * 序列化value
     *
     * @param value
     * @return
     */
    protected <T extends Serializable> byte[] serialize(T value) {
        return serializer.serialize(value);
    }

    /**
     * 反序列化
     *
     * @param bytes
     * @return
     */
    protected <T extends Serializable> T deserialize(byte[] bytes) {
        return serializer.deserialize(bytes);
    }
}
