
package org.scache.cache;

import org.scache.Initializable;


/**
 * 可序列化缓存提供者接口
 *
 * @author 君枫
 * @since 2014年3月11日 下午2:07:18
 */
public interface ISerializableCacheProvider extends Initializable {

    /**
     * 获取缓存
     *
     * @param namespace 命名空间，必须为 {@link CacheNameSpace}定义的
     * @return 非法命名空间返回null
     */
    ISerializableCache getCache(String namespace);

    /**
     * 获取缓存提供者名称
     *
     * @return
     */
    String getName();
}