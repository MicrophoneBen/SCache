
package org.scache;

/**
 * ID生成器接口
 *
 * @author 君枫
 * @version 1.0
 * @since 2014-4-27 上午10:34:27
 */
public interface IdGenerator {

    /**
     * 获取当前ID
     *
     * @return
     */
    String getCurrentId() throws Exception;

    /**
     * 生成ID
     *
     * @param updateCurrentId 是否更新当前ID
     * @return
     */
    String generateId(boolean updateCurrentId) throws Exception;

    /**
     * 更新当前ID
     *
     * @param id
     * @return
     */
    boolean updateCurrentId(String id) throws Exception;

    /**
     * 是否可用
     *
     * @return
     */
    boolean available();
}
